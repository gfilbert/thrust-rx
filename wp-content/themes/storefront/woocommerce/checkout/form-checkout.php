<?php
/**
 * Checkout Form
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/checkout/form-checkout.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.5.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

do_action( 'woocommerce_before_checkout_form', $checkout );

// If checkout registration is disabled and not logged in, the user cannot checkout.
if ( ! $checkout->is_registration_enabled() && $checkout->is_registration_required() && ! is_user_logged_in() ) {
	echo esc_html( apply_filters( 'woocommerce_checkout_must_be_logged_in_message', __( 'You must be logged in to checkout.', 'woocommerce' ) ) );
	return;
}


?>



<div class="timer-box">
            <p class="timer-txt"><span id="viewing"></span> others are viewing this offer right now - <b><span id="stopwatch">00:00:00</span></b> </p>
          </div>
<script>
window.onload = function randomNum() {
  var x = Math.floor((Math.random() * 20) + 13);
  document.getElementById("viewing").innerHTML = x;
}
</script>


<form name="checkout" method="post" class="checkout woocommerce-checkout" action="<?php echo esc_url( wc_get_checkout_url() ); ?>" enctype="multipart/form-data">

	<?php if ( $checkout->get_checkout_fields() ) : ?>

		<?php do_action( 'woocommerce_checkout_before_customer_details' ); ?>

		<div class="col2-set" id="customer_details">
			<div class="col-1">
				<?php do_action( 'woocommerce_checkout_billing' ); ?>
			</div>

			<div class="col-2">
				<?php do_action( 'woocommerce_checkout_shipping' ); ?>
			</div>
		</div>

		<?php do_action( 'woocommerce_checkout_after_customer_details' ); ?>

	<?php endif; ?>
	
	<?php do_action( 'woocommerce_checkout_before_order_review_heading' ); ?>
	
	
	<?php do_action( 'woocommerce_checkout_before_order_review' ); ?>

	<div id="order_review" class="woocommerce-checkout-review-order">
	<h3 id="order_review_heading"><?php esc_html_e( 'Your order', 'woocommerce' ); ?></h3>
		<?php do_action( 'woocommerce_checkout_order_review' ); ?>
	</div>

	<?php do_action( 'woocommerce_checkout_after_order_review' ); ?>

</form>

<?php do_action( 'woocommerce_after_checkout_form', $checkout ); ?>
<script src="../../../../themes/thrustrx/js/jquery.min.js"></script> 
<script src="../../../../themes/thrustrx/js/app.js"></script> 
<script>
				function toggleCardType(cardType) {
					var cardLogos = $('ul.all-card-types li');
					if (cardType && cardType.length > 0) {
						cardLogos.each(function() {
							if ($(this).hasClass(cardType)) {
								$(this).removeClass('off').show();
							} else {
								$(this).addClass('off').hide();
							}
						});
					} else {
						cardLogos.removeClass('off').show();
					}
				}
				function checkoutDownsell() {
					$('input[name=shippingId]').val(213);
					// $('#discounted_price').html('-$6.00');
					$('#total_price').html('$6.94');
					$('#discount-banner').show();
					$('#exitpopup-overlay').fadeOut();
				}
			</script> 
<script>
			  $(document).ready(function () {
			  function blinker() {
			    $('.blink_me').fadeOut(800);
			    $('.blink_me').fadeIn(800);
			  }
			  setInterval(blinker, 1000);
			  });
			</script> 
<script>
				(function() {
					var counter = 3000,
					cDisplay = document.getElementById("stopwatch");
					format = function(t) {
						if (t < 0) {
							cDisplay.innerHTML = '00:00:00';
						} else {
							var minutes = Math.floor(t / 600),
							seconds = Math.floor((t / 10) % 60);
							minutes = (minutes < 10) ? "0" + minutes.toString() : minutes.toString();
							seconds = (seconds < 10) ? "0" + seconds.toString() : seconds.toString();
							cDisplay.innerHTML = minutes + ":" + seconds + ":" + "0" + Math.floor(t % 10);
						}
					};
					setInterval(function() {
						counter--;
						format(counter);
					}, 100);
				})();
			</script> 
<script>
 $(document).ready(function randomNum() {
  var x = Math.floor((Math.random() * 20) + 13);
  document.getElementById("demo").innerHTML = x;
});
</script>

			<script>
			document.getElementById("watch-number").onload = function() {randomNum()};
			</script>
