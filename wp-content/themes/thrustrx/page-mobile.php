
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="https://gmpg.org/xfn/11">
	<?php wp_head(); ?>
	
<title>Thrust Rx</title>
<meta name="description" content="Thrust Rx">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta http-equiv="content-type" content="text/html;charset=utf-8">
<meta http-equiv="content-language" content="en-us">
<meta name="apple-mobile-web-app-capable" content="yes">
<meta name="apple-mobile-web-app-status-bar-style" content="black">
<meta name="HandheldFriendly" content="true">
<link rel="stylesheet" href="https://thrustrx.com/wp-content/themes/thrustrx/m/css/app.css">

   <meta name="viewport" content="width=640 , user-scalable=0">
  <title>Thrust Rx™ Male Enhancement</title>
  <meta name="description" content="">
  <meta name="keywords" content="">
         <link rel="stylesheet" href="https://thrustrx.com/wp-content/themes/thrustrx/m/css/style-index.css">
      <!--   <script src="js/main.js"></script> -->
        <style type="text/css">
         .sprite {
          background-image: url(https://thrustrx.com/wp-content/themes/thrustrx/m/images/spritesheet.png);
          background-repeat: no-repeat;
          display: block;
        }
        .sprite-line2 {
          width: 628px;
          height: 7px;
          background-position: -5px -5px;
        }
        .sprite-listimg {
          width: 39px;
          height: 43px;
          background-position: -5px -22px;
        }
        .sprite-next {
          width: 35px;
          height: 51px;
          background-position: -54px -22px;
        }
        .sprite-plus {
          width: 12px;
          height: 12px;
          background-position: -99px -22px;
        }
        .sprite-prev {
          width: 35px;
          height: 51px;
          background-position: -121px -22px;
        }
        .sprite-s3-line {
          width: 533px;
          height: 10px;
          background-position: -5px -83px;
        }
        .sprite-stars {
          width: 95px;
          height: 16px;
          background-position: -166px -22px;
        }
        </style>
</head>

<body <?php body_class(); ?>>
<div id="page" class="site">
	<a class="skip-link screen-reader-text" href="#content"><?php esc_html_e( 'Skip to content', 'thrustrx' ); ?></a>

	<header id="masthead" class="site-header">
		<div class="site-branding">
			<?php
			the_custom_logo();
			if ( is_front_page() && is_home() ) :
				?>
				<h1 class="site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></h1>
				<?php
			else :
				?>
				<p class="site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></p>
				<?php
			endif;
			$thrustrx_description = get_bloginfo( 'description', 'display' );
			if ( $thrustrx_description || is_customize_preview() ) :
				?>
				<p class="site-description"><?php echo $thrustrx_description; /* WPCS: xss ok. */ ?></p>
			<?php endif; ?>
		</div><!-- .site-branding -->

		<nav id="site-navigation" class="main-navigation">
			<button class="menu-toggle" aria-controls="primary-menu" aria-expanded="false"><?php esc_html_e( 'Primary Menu', 'thrustrx' ); ?></button>
			<?php
			wp_nav_menu( array(
				'theme_location' => 'menu-1',
				'menu_id'        => 'primary-menu',
			) );
			?>
		</nav><!-- #site-navigation -->
	</header><!-- #masthead -->

	<div id="content" class="site-content">

	<div id="primary" class="content-area">
		<main id="main" class="site-main">

		<?php
		if ( have_posts() ) :

			if ( is_home() && ! is_front_page() ) :
				?>
				<header>
					<h1 class="page-title screen-reader-text"><?php single_post_title(); ?></h1>
				</header>
				<?php
			endif;

			/* Start the Loop */
			while ( have_posts() ) :
				the_post();

				/*
				 * Include the Post-Type-specific template for the content.
				 * If you want to override this in a child theme, then include a file
				 * called content-___.php (where ___ is the Post Type name) and that will be used instead.
				 */
				get_template_part( 'template-parts/content', get_post_type() );

			endwhile;

			the_posts_navigation();

		else :

			get_template_part( 'template-parts/content', 'none' );

		endif;
		?>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_sidebar();
get_footer();
