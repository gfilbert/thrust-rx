=== Stripe Payments Subscriptions Addon ===
Contributors: alexanderfoxc
Donate link: https://stripe-plugins.com
Tags: stripe
Requires at least: 4.7
Tested up to: 5.3
Stable tag: 2.0.2

Adds Stripe Subscriptions support to the core plugin.

== Description ==

Adds Stripe Subscriptions support to the core plugin.

== Changelog ==

= 2.0.2 =
* Fixed some symbols in plan names were incorrectly displayed on Stripe Dashboard.

= 2.0.1 =
* Fixed subscriptions with tax issues. Requires Stripe Payments 2.0.12+.

= 2.0.0 =
* Added Stripe SCA support.

= 1.5.5 =
* Customer name and billing address are added to Stripe Dashboard. Works only if "Collect Address on Checkout" option is enabled in product settings.

= 1.5.4 =
* Added functions for other addons to handle subscription ended or canceled actions.
This allows addons like MailChimp, ConvertKit etc. to unsubscribe customers when subscription ended.
* Minor text updates.

= 1.5.3 =
* Minor bugfixes to prevent PHP notices.

= 1.5.2 =
* Fixed link to Settings page in webhooks notification message.

= 1.5.1 =
* Fixed tax percent was improperly rounded down.

= 1.5.0 =
* Implemented credit card update functionality. Check out [documentation](https://s-plugins.com/stripe-subscription-payments-addon/#cc-update)
* Added configurable option to email customer when his\her credit card is close to expiry (Stripe Payments -> Settings, Subscriptions tab).
* Added update credit card URL to subscription details screen.
* Added {sub_update_cc_url} email shortcode that produces update credit card link for a customer.
* Subscriptions dates are now displayed using WP date settings and considers timezone.
* Assigned text domain and generated .pot file for translation purposes.

= 1.4.7 =
* Fixed race condition when "Create Webhook" buttons were clicked in a row.
* Fixed Subscription Plans and Subscriptions tables column sorting.
* Fixed minor HTML issues on Settings page that could produce improper settings tab display in some browsers.

= 1.4.6 =
* Fixed negative amount display in email receipts and checkout results page when coupon is used with trial subscriptions.
Requires core plugin version 1.9.18+

= 1.4.5 =
* Trial subscriptions are now displaying 0 as payment amount on checkout results and email receipts.
Payment button in Stripe pop-up for those now shows "Start Free Trial" instead of payment amount.
Requires core plugin version 1.9.18+

= 1.4.4 =
* Added {sub_cancel_url} email merge tag that allows you to send cancellation URL to your customers.
Requires core plugin version 1.9.17+ to function properly.
* Added cancellation URL to subscription details screen.
* Added core plugin's [asp_show_my_transactions] shortcode support.

= 1.4.3 =
* Fixed webhooks notice was displayed even if webhooks were properly created.

= 1.4.2 =
* Fixed total amount display on checkout results page and emails when coupon is used.

= 1.4.1 =
* Fixed all new plans were created in Test mode under some circumstances.
* Fixed PHP notices on subscription plan update.

= 1.4.0 =
* Added automatic webhook creation functionality on settings page.
* Added admin notice if webhooks are not configured.

= 1.3.5 =
* Fixed Stripe mode was improperly set for subscription plans during payment processing.

= 1.3.4 =
* Added variable amount and currency support for Subscriptions (requires Stripe Payments 1.9.14+).
* Fixed plan mode wasn't properly indicated sometimes in Subscriptions list.

= 1.3.3 =
* Added quantity consideration for subscriptions (requires Stripe Payments 1.9.14+).
* Added Coupons support for subscriptions (requires Stripe Payments 1.9.14+).

= 1.3.2 =
* Webhooks URLs on the settings page are now displayed with "https://" regardless of the "WordPress Address" setting.

= 1.3.1 =
* Fixed rare race condition issue when first plan payment could get not counted for eMember subscriptions.
* Made debug output less aggressive.

= 1.3.0 =
* Error is displayed when trying to create a plan with negative numeric values for some inputs (Amount, Duration etc).
* Fixed a couple of PHP Notices.

= 1.2.9 =
* Plan name is now used as product name when creating or updating plans. This is to make sure customers get correct product name in Stripe's receipt email.
* Added notice regarding plan modes on plan create\edit page. Made Mode radioboxes larger and easier to click.

= 1.2.8 =
* Added eMember integration support (requires core plugin version 1.9.3+).
* Fixed PHP Notice during checkout process.

= 1.2.7 =
* Added the asp_subscription_invoice_paid action hook.
* Added the asp_subscription_ended action hook.
* Added the asp_subscription_canceled action hook.

= 1.2.6 =
* Webhook signature is now only checked if received webhook type is of one those that needs to be processed.

= 1.2.5 =
* Fixed webhook signature check was failing under some cirsumstances.
* Fixed currency was improperly taken from product setting rather than plan setting.

= 1.2.4 =
* Fixed "Can't find plan ID" error if plans were created in older addon version.

= 1.2.3 =
* Fixed plan data couldn't be fetched from Stripe under some circumstances.

= 1.2.2 =
* Added link to "Add new plan" page if no plans created.

= 1.2.1 =
* Ended subscriptions are no longer displayed as "Canceled" now.
* Fixed typos.

= 1.2 =
* Fixed PHP fatal error when core plugin is not installed or not active.
* Plugin now checks for minimum required core plugin version (1.8.4.) and shows error message if it's lower.
* Enabled addon update functionality.

= 1.1 =
* The subscription amount description now gets displayed on the front-end product box.

= 1.0 =
* First public test release.