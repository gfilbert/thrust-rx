var tokenId = '';
var si_id = '';
var si_cs = '';
var form = document.getElementById('update-form');
var submitBtn = document.getElementById('submitBtn');
var tokenInput = document.getElementById('pm_id');
var stripe = Stripe(vars.key);
var elements = stripe.elements();
var cardErrorCont = document.getElementById('card-errors');
var errorCont = document.getElementById('error-cont');
var style = {
	base: {
		fontSize: '16px',
	}
};
var card = elements.create('card', {
	style: style,
	hidePostalCode: true
});

card.on('ready', function () {
	submitBtn.disabled = false;
});

card.mount('#card-element');

card.addEventListener('change', function (event) {
	if (event.error) {
		cardErrorCont.textContent = event.error.message;
	} else {
		cardErrorCont.textContent = '';
	}
});
form.addEventListener('submit', function (event) {
	event.preventDefault();
	if (tokenInput.value !== '') {
		form.submit();
		return true;
	}
	event.preventDefault();
	submitBtn.disabled = true;

	stripe.createToken(card).then(function (result) {
		console.log(result);
		if (result.error) {
			submitBtn.disabled = false;
		} else {
			tokenId = result.token.id;
			var reqStr = 'action=asp_sub_create_si&cust_id=' + vars.custId + '&token_id=' + tokenId + '&is_live=' + vars.isLive;
			console.log('Doing action asp_sub_create_si');
			new ajaxRequest(vars.ajaxUrl, reqStr,
				function (res) {
					try {
						var resp = JSON.parse(res.responseText);
						console.log(resp);
						if (resp.err !== '') {
							submitBtn.disabled = false;
							errorCont.innerHTML = resp.err;
							return false;
						}
						si_id = resp.si_id;
						si_cs = resp.si_cs;
						console.log('Doing handleCardSetup()');
						stripe.handleCardSetup(si_cs, card).then(function (result) {
							console.log(result);
							if (result.error) {
								errorCont.innerHTML = result.error.message;
								submitBtn.disabled = false;
								return false;
							}
							tokenInput.value = result.setupIntent.payment_method;
							form.dispatchEvent(new Event('submit'));
						});
					} catch (e) {
						console.log(e);
						alert('Caught Exception: ' + e.description);
					}
				},
				function (res, errMsg) {
					submitBtn.disabled = false;
					errorCont.innerHTML = errMsg;
				}
			);
		}
	});
});
