<?php

/**
 * Plugin Name: Stripe Payments Subscriptions Addon
 * Plugin URI: https://s-plugins.com/
 * Description: Adds Stripe Subscriptions support to the core plugin.
 * Version: 2.0.2
 * Author: Tips and Tricks HQ, alexanderfoxc
 * Author URI: https://s-plugins.com/
 * Text Domain: asp-sub
 * Domain Path: /languages
 * License: GPL2
 */
if ( ! defined( 'ABSPATH' ) ) {
	exit; //Exit if accessed directly
}

class ASPSUB_main {


	protected static $instance = null;
	protected $plugin_uid      = false;
	static $plans_slug         = 'asp_sub_plan';
	static $subs_slug          = 'asp_sub_subs';

	const ADDON_VER = '2.0.2';

	var $helper;
	var $file;
	var $ADDON_SHORT_NAME  = 'Sub';
	var $ADDON_FULL_NAME   = 'Stripe Payments Subscriptions Addon';
	var $MIN_ASP_VER       = '1.9.12';
	var $SLUG              = 'stripe-payments-subscriptions';
	var $SETTINGS_TAB_NAME = 'sub';
	var $textdomain        = 'asp-sub';
	var $ASPMain;
	var $PLUGIN_DIR;
	var $plan;

	public static function get_instance() {
		// If the single instance hasn't been set, set it now.
		if ( null === self::$instance ) {
			self::$instance = new self();
		}

		return self::$instance;
	}

	public function __construct() {
		$this->PLUGIN_DIR = plugin_dir_path( __FILE__ );
		$this->file       = __FILE__;
		add_action( 'plugins_loaded', array( $this, 'plugins_loaded' ) );
		register_activation_hook( __FILE__, array( 'ASPSUB_main', 'activate' ) );
		register_deactivation_hook( __FILE__, array( 'ASPSUB_main', 'deactivate' ) );
	}

	public static function get_plan_descr( $post_id, $need_total = false ) {
		$variables = get_post_meta( $post_id, 'asp_sub_plan_variables', true );
		$curr      = get_post_meta( $post_id, 'asp_sub_plan_currency', true );
		$price     = get_post_meta( $post_id, 'asp_sub_plan_price', true );
		$price_fmt = AcceptStripePayments::formatted_price( $price, $curr );
		if ( isset( $variables['price'] ) ) {
			$price_fmt = 'N';
		}
		$interval_count = get_post_meta( $post_id, 'asp_sub_plan_period', true );
		$interval       = get_post_meta( $post_id, 'asp_sub_plan_period_units', true );
		$duration       = get_post_meta( $post_id, 'asp_sub_plan_duration', true );
		$trial          = get_post_meta( $post_id, 'asp_sub_plan_trial', true );
		$str            = $price_fmt;
		$interval       = ASPSUB_Utils::get_interval_str( $interval, $interval_count );
		if ( $interval_count == 1 ) {
			$str .= '/' . $interval;
		} else {
			$str .= ' ' . __( 'every', 'asp-sub' ) . ' ' . $interval_count . ' ' . $interval;
		}
		if ( $duration != 0 ) {
			$str .= ' X ' . $duration;
			if ( $need_total && ! isset( $variables['price'] ) ) {
				$str .= '<br />' . __( 'Total:', 'asp-sub' ) . ' ' . AcceptStripePayments::formatted_price( $duration * $price, $curr );
			}
		} else {
			$str .= ' ' . __( 'until cancelled', 'asp-sub' );
		}
		if ( $trial ) {
			$trial_interval_str = ASPSUB_Utils::get_interval_str( 'days', $trial );
			$str                = __( sprintf( 'Free for %d %s, then', $trial, $trial_interval_str ), 'asp-sub' ) . ' ' . $str;
		}
		return $str;
	}

	public function get_uid() {
		if ( ! empty( $this->plugin_uid ) ) {
			return $this->plugin_uid;
		}
		$uid = get_option( 'asp_sub_plugin_uid' );
		if ( ! $uid ) {
			$uid              = uniqid();
			$this->plugin_uid = $uid;
			update_option( 'asp_sub_plugin_uid', $uid );
		}
		return $uid;
	}

	public function settings_link( $links, $file ) {
		if ( $file === plugin_basename( __FILE__ ) ) {
			$settings_link = '<a href="edit.php?post_type=asp-products&page=asp-sub-settings#sub">' . __( 'Settings', 'stripe-payments' ) . '</a>';
			array_unshift( $links, $settings_link );
		}
		return $links;
	}

	public static function deactivate() {
		ASPSUB_Utils::get_instance()->process_deactivate_hook();
	}

	public static function activate() {
		$opt      = get_option( 'AcceptStripePayments-settings' );
		$defaults = array(
			'sub_expiry_email_enabled' => 0,
			'sub_expiry_email_from'    => get_bloginfo( 'name' ) . ' <sales@your-domain.com>',
			'sub_expiry_email_subj'    => __( 'Your credit card is going to expire soon', 'asp-sub' ),
			'sub_expiry_email_body'    => __( "Hello.\n\nYour credit card {card_brand} ending in {card_last_4} is going to expire on {card_exp_month}/{card_exp_year}.\n\nPlease click the link below to update it:\n{update_cc_url}", 'asp-sub' ),
		);
		$new_opt  = array_merge( $defaults, $opt );
		// unregister setting to prevent main plugin from sanitizing our new values
		unregister_setting( 'AcceptStripePayments-settings-group', 'AcceptStripePayments-settings' );
		update_option( 'AcceptStripePayments-settings', $new_opt );
	}

	public function plugins_loaded() {
		if ( class_exists( 'AcceptStripePayments' ) ) {

			$this->ASPMain = AcceptStripePayments::get_instance();
			$this->helper  = new ASPAddonsHelper( $this );
			//check minimum required core plugin version
			if ( ! $this->helper->check_ver() ) {
				return false;
			}
			$this->helper->init_tasks();

			include_once plugin_dir_path( __FILE__ ) . 'admin/asp-sub-plans-post-type.php';
			$ASPSUB_plans = ASPSUB_plans::get_instance();
			add_action( 'init', array( $ASPSUB_plans, 'register_post_type' ), 100 );
			include_once plugin_dir_path( __FILE__ ) . 'admin/asp-sub-subs-post-type.php';
			$ASPSUB_subs = ASPSUB_subs::get_instance();
			add_action( 'init', array( $ASPSUB_subs, 'register_post_type' ), 100 );

			$this->ASPMain = AcceptStripePayments::get_instance();

			include_once plugin_dir_path( __FILE__ ) . 'inc/asp-sub-utils-class.php';

			//check if this is webhook post from Stripe
			if ( isset( $_GET['asp_hook'] ) ) {
				include_once $this->PLUGIN_DIR . 'inc/asp-sub-stripe-webhooks.php';
			}
			if ( wp_doing_ajax() ) {
				add_action( 'wp_ajax_asp_pp_confirm_token', array( $this, 'confirm_token' ) );
				add_action( 'wp_ajax_nopriv_asp_pp_confirm_token', array( $this, 'confirm_token' ) );
				add_action( 'wp_ajax_nopriv_asp_cancel_sub', array( $this, 'process_subs_cancel' ) );
				add_action( 'wp_ajax_asp_cancel_sub', array( $this, 'process_subs_cancel' ) );
				add_action( 'wp_ajax_asp_sub_create_si', array( $this, 'create_si' ) );
				add_action( 'wp_ajax_nopriv_asp_sub_create_si', array( $this, 'create_si' ) );
			}
			//check if we need to handle sub cancel url
			$asp_action = filter_input( INPUT_GET, 'asp_sub_action', FILTER_SANITIZE_STRING );
			if ( $asp_action === 'cancel' ) {
				$this->cancel_url_handler();
			} elseif ( $asp_action === 'update' ) {
				$this->update_cc_url_handler();
			}

			add_filter( 'asp_ng_process_ipn_payment_data_item_override', array( $this, 'payment_data_item_override' ), 10, 2 );
			add_filter( 'asp_ng_process_ipn_product_item_override', array( $this, 'product_item_override' ) );
			add_filter( 'asp_email_body_tags_vals_before_replace', array( $this, 'email_body_tags_vals_before_replace' ), 10, 2 );
			add_filter( 'asp_email_body_after_replace', array( $this, 'email_body_after_replace' ) );
			add_filter( 'asp_sc_show_user_transactions_additional_data', array( $this, 'show_user_transactions_handler' ), 10, 3 );
			add_filter( 'asp_ng_before_token_create', array( $this, 'before_token_create' ), 10, 2 );
			add_filter( 'asp_ng_item_price_before_token_create', array( $this, 'item_price_before_token_create' ), 10, 2 );

			if ( ! is_admin() ) {
				//		add_filter( 'asp-button-output-additional-styles', array( $this, 'output_styles' ) );
				add_filter( 'asp-button-output-data-ready', array( $this, 'data_ready' ), 10, 2 );
				add_filter( 'asp_ng_button_output_data_ready', array( $this, 'data_ready' ), 10, 2 );
				add_filter( 'asp_product_tpl_tags_arr', array( $this, 'product_tpl_tags' ), 10, 2 );
				add_filter( 'asp_process_charge', array( $this, 'process_charge' ), 10, 1 );
				add_filter( 'asp_before_payment_processing', array( $this, 'before_payment_processing' ), 10, 2 );
				//		add_filter( 'asp-button-output-after-button', array( $this, 'after_button' ), 10, 3 );
				//		add_action( 'asp-button-output-register-script', array( $this, 'register_script' ) );
				//		add_action( 'asp-button-output-enqueue-script', array( $this, 'enqueue_script' ) );
				//		add_action( 'asp-before-payment-processing', array( $this, 'before_payment_processing' ) );

				add_filter( 'asp_order_before_insert', array( $this, 'order_before_insert' ), 10, 3 );
				add_filter( 'asp_ng_payment_completed', array( $this, 'payment_completed' ), 10, 2 );
			} else {
				include_once plugin_dir_path( __FILE__ ) . 'admin/asp-sub-admin-menu.php';
				$ASPSUB_admin_menu = new ASPSUB_admin_menu();
				add_filter( 'asp_products_table_price_column', array( $this, 'products_table_price_column' ), 10, 4 );
			}
		}
	}

	public function product_item_override( $item ) {
		$sub_id = filter_input( INPUT_POST, 'asp_sub_id', FILTER_SANITIZE_STRING );
		if ( is_null( $sub_id ) ) {
			return $item;
		}

		$prod_id = $item->get_product_id();
		require_once $this->PLUGIN_DIR . 'inc/class-asp-sub-product-item.php';

		$sub_item = new ASP_Sub_Product_Item( $prod_id );

		if ( $sub_item->get_last_error() ) {
			return $item;
		}

		$plan_id = $sub_item->get_plan_id();

		if ( ! $plan_id ) {
			return $item;
		}

		$plan = $this->get_plan_data( $plan_id );

		if ( isset( $plan->is_variable ) && $plan->is_variable ) {
			if ( isset( $plan->variables['price'] ) ) {
				$posted_amount = filter_input( INPUT_POST, 'asp_amount', FILTER_SANITIZE_NUMBER_INT );
				if ( $posted_amount ) {
					$plan->amount = AcceptStripePayments::from_cents( $posted_amount, $plan->currency );
					$sub_item->set_price( $plan->amount );
				}
			}
		}

		$this->log( 'Overriding product item.' );

		return $sub_item;
	}

	public function payment_data_item_override( $p_data, $pi ) {
		$sub_id = filter_input( INPUT_POST, 'asp_sub_id', FILTER_SANITIZE_STRING );
		if ( is_null( $sub_id ) ) {
			return $p_data;
		}

		require_once $this->PLUGIN_DIR . 'inc/class-asp-sub-payment-data.php';
		$t_p_data = new ASP_Sub_Payment_Data( $sub_id );
		if ( $pi !== $sub_id ) {
			$t_p_data->set_pid( $pi );
		} else {
			// we need to handle metadata as there is no PaymentIntent created for this subscription
			$this->sub_id = $sub_id;
			add_filter( 'asp_ng_handle_metadata', array( $this, 'handle_metadata' ) );
		}

		$this->log( 'Overriding payment data.' );

		return $t_p_data;
	}

	public function handle_metadata( $metadata ) {
		$this->log( 'Handling metadata.' );
		\Stripe\Subscription::update( $this->sub_id, array( 'metadata' => $metadata ) );
		return true;
	}

	public function confirm_token() {
		include_once $this->PLUGIN_DIR . 'inc/asp-sub-stripe-subs-class.php';

		$out            = array();
		$out['success'] = false;

		$prod_id = filter_input( INPUT_POST, 'product_id', FILTER_SANITIZE_NUMBER_INT );
		if ( empty( $prod_id ) ) {
			$out['err'] = 'Invalid product id provided.';
			wp_send_json( $out );
		}

		$token = filter_input( INPUT_POST, 'asp_token_id', FILTER_SANITIZE_STRING );
		if ( empty( $token ) ) {
			$out['err'] = 'Invalid token provided.';
			wp_send_json( $out );
		}

		$plan_id = get_post_meta( $prod_id, 'asp_sub_plan_id', true );

		if ( ! $plan_id ) {
			$out['err'] = 'No plan ID specified.';
			wp_send_json( $out );
		}

		$this->log( 'Starting pre-subscription process.' );

		$plan = $this->get_plan_data( $plan_id );

		//handle variable currency
		if ( isset( $plan->is_variable ) && $plan->is_variable && ! empty( $plan->variables['currency'] ) ) {
			$plan->currency = filter_input( INPUT_POST, 'currency', FILTER_SANITIZE_STRING );
		}

		$currency_code = strtoupper( $plan->currency );

		//check if this is variable plan
		if ( isset( $plan->is_variable ) && $plan->is_variable ) {
			//we need to create a plan first
			include_once self::get_plugin_dir() . 'inc/asp-sub-stripe-plans-class.php';

			$stripe_plans = new ASPSUB_stripe_plans( $plan->livemode );
			//let's create pricing plan in Stripe
			$plan_opts = array(
				'currency'          => $plan->currency,
				'interval'          => $plan->interval,
				'interval_count'    => $plan->interval_count,
				'amount'            => $plan->amount,
				'nickname'          => $plan->nickname,
				'trial_period_days' => 0,
			);

			$posted_amount = filter_input( INPUT_POST, 'amount', FILTER_SANITIZE_NUMBER_INT );
			if ( $posted_amount ) {
				$plan_opts['amount'] = AcceptStripePayments::from_cents( $posted_amount, $plan->currency );
			}

			$plan = $stripe_plans->create( $plan_opts, $plan->post_id );
		}

		$items = array( 'plan' => $plan->id );

		$item_quantity = false;

		if ( isset( $data['item_quantity'] ) && ! empty( $data['item_quantity'] ) ) {
			$item_quantity = intval( $data['item_quantity'] );
		}

		if ( $item_quantity ) {
			$items['quantity'] = $item_quantity;
		}

		//preload subscriptions post-related class
		$sub = new ASPSUB_stripe_subs( $plan->livemode );

		$post_billing_details = filter_input( INPUT_POST, 'billing_details', FILTER_SANITIZE_STRING );

		$post_shipping_details = filter_input( INPUT_POST, 'shipping_details', FILTER_SANITIZE_STRING );

		try {

			ASPMain::load_stripe_lib();

			\Stripe\Stripe::setApiKey( $plan->livemode ? $this->ASPMain->APISecKeyLive : $this->ASPMain->APISecKeyTest );

			$customer_opts = array(
				'source' => $token,
			);

			if ( isset( $post_billing_details ) ) {
				$post_billing_details = html_entity_decode( $post_billing_details );

				$billing_details = json_decode( $post_billing_details );

				if ( $billing_details->name ) {
					$customer_opts['name'] = $billing_details->name;
				}

				if ( $billing_details->email ) {
					$customer_opts['email'] = $billing_details->email;
				}

				if ( isset( $billing_details->address ) && isset( $billing_details->address->line1 ) ) {
					//we have address
					$addr = array(
						'line1'   => $billing_details->address->line1,
						'city'    => isset( $billing_details->address->city ) ? $billing_details->address->city : null,
						'country' => isset( $billing_details->address->country ) ? $billing_details->address->country : null,
					);

					if ( isset( $billing_details->address->postal_code ) ) {
						$addr['postal_code'] = $billing_details->address->postal_code;
					}

					$customer_opts['address'] = $addr;
				}
			}

			if ( isset( $post_shipping_details ) ) {
				$post_shipping_details = html_entity_decode( $post_shipping_details );

				$shipping_details = json_decode( $post_shipping_details );

				$shipping = array();

				if ( $shipping_details->name ) {
					$shipping['name'] = $shipping_details->name;
				}

				if ( isset( $shipping_details->address ) && isset( $shipping_details->address->line1 ) ) {
					//we have address
					$addr = array(
						'line1'   => $shipping_details->address->line1,
						'city'    => isset( $shipping_details->address->city ) ? $shipping_details->address->city : null,
						'country' => isset( $shipping_details->address->country ) ? $shipping_details->address->country : null,
					);

					if ( isset( $shipping_details->address->postal_code ) ) {
						$addr['postal_code'] = $shipping_details->address->postal_code;
					}

					$shipping['address'] = $addr;

					$customer_opts['shipping'] = $shipping;
				}
			}

			$customer = \Stripe\Customer::create( $customer_opts );

			$sub_opts = array(
				'customer'                   => $customer->id,
				'enable_incomplete_payments' => true,
				'items'                      => array( $items ),
				'trial_from_plan'            => true,
				'expand'                     => array( 'latest_invoice.payment_intent', 'pending_setup_intent' ),
			);

			$tax = get_post_meta( $prod_id, 'asp_product_tax', true );

			if ( ! empty( $tax ) ) {
				$sub_opts['tax_percent'] = floatval( $tax );
			}

			$subscription = \Stripe\Subscription::create( $sub_opts );
		} catch ( Exception $e ) {
			$this->log( 'Stripe API error occurred: ' . $e->getMessage(), false );
			$out['err'] = $e->getMessage();
			wp_send_json( $out );
		}

		$sub_post_id = $sub->insert( $subscription, $customer, $plan_id, $prod_id, array() );

		if ( 'incomplete' === $subscription->status ) {
			$out['pi_cs'] = $subscription->latest_invoice->payment_intent->client_secret;
		}

		if ( 'trialing' === $subscription->status ) {
			if ( $subscription->pending_setup_intent ) {
				$out['pi_cs']         = $subscription->pending_setup_intent->client_secret;
				$out['pi_id']         = $subscription->id;
				$out['do_card_setup'] = true;
			} else {
				$out['no_action_required'] = true;
				$out['pi_id']              = $subscription->id;
			}
		} else {
			$out['pi_id'] = $subscription->latest_invoice->payment_intent->id;
		}

		$out['sub_id'] = $subscription->id;

		//      $out['err']=array($subscription);
		//      wp_send_json($out);

		$this->log( sprintf( 'Pre-subscription completed for plan "%s" (%s)', $plan_id, $plan->nickname ) );

		$out['success'] = true;
		wp_send_json( $out );
	}

	public function payment_completed( $data, $prod_id ) {
		$plan_id = get_post_meta( $prod_id, 'asp_sub_plan_id', true );

		if ( ! $plan_id ) {
			// no plan id found, this is not subscriptions product
			// $this->log( 'No plan ID specified.', false );
			return $data;
		}

		$sub_id = filter_input( INPUT_POST, 'asp_sub_id', FILTER_SANITIZE_STRING );

		if ( ! $sub_id ) {
			$this->log( 'No sub ID provided.', false );
			return $data;
		}

		$plan = $this->get_plan_data( $plan_id );

		include_once $this->PLUGIN_DIR . 'inc/asp-sub-stripe-subs-class.php';

		$sub = new ASPSUB_stripe_subs( $plan->livemode );

		ASPMain::load_stripe_lib();

		\Stripe\Stripe::setApiKey( $plan->livemode ? $this->ASPMain->APISecKeyLive : $this->ASPMain->APISecKeyTest );

		$subscription = \Stripe\Subscription::retrieve( $sub_id );
		$customer     = \Stripe\Customer::retrieve( $subscription->customer );

		$currency_code = strtoupper( $plan->currency );

		$item_quantity = false;

		if ( isset( $data['item_quantity'] ) && ! empty( $data['item_quantity'] ) ) {
			$item_quantity = intval( $data['item_quantity'] );
		}

		//      $sub_post_id = $sub->insert( $subscription, $customer, $plan_id, $prod_id, array() );

		$data['charge'] = $subscription;

		$amount = $plan->amount;

		if ( ! empty( $plan->trial_period_days ) ) {
			$amount           = 0;
			$data['is_trial'] = true;
		}

		$item_price = $amount;

		if ( $amount > 0 ) {

			$discount_amount = ! empty( $coupon['discountAmount'] ) ? AcceptStripePayments::is_zero_cents( $currency_code ) ? $coupon['discountAmount'] : $coupon['discountAmount'] * 100 : 0;

			if ( ! empty( $discount_amount ) ) {
				$amount = $amount - $discount_amount;
			}
		}

		if ( ! empty( $tax ) ) {
			$amount = round( AcceptStripePayments::apply_tax( $amount, $tax ), 0 );
		}

		$amount = ! AcceptStripePayments::is_zero_cents( $currency_code ) ? $amount / 100 : $amount;

		if ( $item_quantity ) {
			$amount = $amount * $item_quantity;
		}

		if ( isset( $data['addonName'] ) ) {
			$data['addonName'] .= '[SUB]';
		} else {
			$data['addonName'] = '[SUB]';
		}

		$data['is_live'] = $plan->livemode ? true : false;

		return $data;
	}

	public function products_table_price_column( $output, $o_price, $o_currency, $post_id ) {
		$plan_id = get_post_meta( $post_id, 'asp_sub_plan_id', true );
		if ( empty( $plan_id ) ) {
			return $output;
		}
		$plan = $this->get_plan_data( $plan_id );
		if ( $plan === false ) {
			return sprintf( '<span style="color: red">Can\'t find plan with ID %s</span>', $plan_id );
		}
		$str = 'Plan: ' . '<br /><i>' . $plan->nickname . '</i>';

		$price = $plan->amount;

		if ( ! in_array( strtoupper( $plan->currency ), $this->ASPMain->zeroCents ) ) {
			$price = $price / 100;
		}

		$str .= '<br/ >' . self::get_plan_descr( $plan_id );

		return $str;
	}

	public function product_tpl_tags( $tags, $id ) {
		$plan_id = get_post_meta( $id, 'asp_sub_plan_id', true );
		if ( ! $plan_id ) {
			return $tags;
		}
		$plan = $this->get_plan_data( $plan_id );
		if ( ! $plan ) {
			return $tags;
		}
		$price = $plan->amount;

		if ( ! in_array( strtoupper( $plan->currency ), $this->ASPMain->zeroCents ) ) {
			$price = $price / 100;
		}

		$tags['price'] = self::get_plan_descr( $plan_id );

		$tax = get_post_meta( $id, 'asp_product_tax', true );

		$under_price_line = '';

		if ( $tax ) {
			$tax_amount       = round( ( $price * $tax / 100 ), 2 );
			$under_price_line = '<span class="asp_price_tax_section">' . AcceptStripePayments::formatted_price( $tax_amount, $plan->currency ) . __( ' (tax per payment)', 'asp-sub' ) . '</span>';
		}

		if ( ! empty( $under_price_line ) ) {
			//$under_price_line .= '<div class="asp_price_full_total">' . __( 'Total:', 'stripe-payments' ) . ' ' . AcceptStripePayments::formatted_price( $price + $tax_amount, $plan->currency ) . '</div>';
		}

		$tags['under_price_line'] = $under_price_line;

		return $tags;
	}

	public function before_payment_processing( $res, $post ) {
		$this->log( 'Starting pre-subscription process.' );

		if ( empty( $post['stripeProductId'] ) ) {
			$this->log( 'No product ID specified.', false );
			return;
		}
		$id = $post['stripeProductId'];

		$plan_id = get_post_meta( $id, 'asp_sub_plan_id', true );

		if ( ! $plan_id ) {
			$this->log( 'No plan ID specified.', false );
			return;
		}

		$plan = $this->get_plan_data( $plan_id );

		if ( ! $plan ) {
			$this->log( 'Can\'t get plan data.', false );
			return;
		}
		$amount                = ! AcceptStripePayments::is_zero_cents( $plan->currency ) ? $plan->amount / 100 : $plan->amount;
		$_POST['stripeAmount'] = $amount;

		$this->plan = $plan;
		return;
	}

	public function get_plan_data( $plan_id ) {
		//get Stripe lib
		//it must be loaded before getting plan data from cache, otherwise cached plan data object would become __PHP_Incomplete_Class
		ASPMain::load_stripe_lib();

		$is_live     = get_post_meta( $plan_id, 'asp_sub_plan_is_live', true );
		$is_variable = get_post_meta( $plan_id, 'asp_sub_plan_is_variable', true );

		if ( $is_variable ) {
			//this is variable plan. Let's return its data
			$plan_post                  = get_post( $plan_id );
			$plan                       = new stdClass();
			$plan->currency             = get_post_meta( $plan_id, 'asp_sub_plan_currency', true );
			$plan->interval             = get_post_meta( $plan_id, 'asp_sub_plan_period_units', true );
			$plan->interval_count       = get_post_meta( $plan_id, 'asp_sub_plan_period', true );
			$plan->name                 = $plan_post->post_title;
			$plan->nickname             = $plan_post->post_title;
			$plan->statement_descriptor = null;
			$plan->is_variable          = true;
			$plan->livemode             = $is_live;
			$plan->post_id              = $plan_id;
			$plan->variables            = get_post_meta( $plan_id, 'asp_sub_plan_variables', true );
			if ( isset( $plan->variables['price'] ) && ( isset( $_POST['stripeAmount'] ) || isset( $_POST['asp_amount'] ) ) ) {
				if ( isset( $_POST['stripeAmount'] ) ) {
					$amt = floatval( $_POST['stripeAmount'] );
				}
				if ( empty( $amt ) && isset( $_POST['asp_amount'] ) ) {
					$amt = floatval( $_POST['asp_amount'] );
					if ( ! AcceptStripePayments::is_zero_cents( $plan->currency ) ) {
						$amt = round( $amt * 100 );
					}
				}
				$plan->amount = $amt;
			} else {
				if ( ! isset( $plan->variables['price'] ) ) {
					$plan->amount = get_post_meta( $plan_id, 'asp_sub_plan_price', true );
					if ( ! in_array( strtoupper( $plan->currency ), $this->ASPMain->zeroCents ) ) {
						$plan->amount = $plan->amount * 100;
					}
				} else {
					$plan->amount = 0;
				}
			}
			return $plan;
		}

		//convert plan post ID to stripe plan ID
		$plan_id = get_post_meta( $plan_id, 'asp_sub_stripe_plan_id', true );

		if ( $is_live ) {
			$key = $this->ASPMain->APISecKeyLive;
		} else {
			$key = $this->ASPMain->APISecKeyTest;
		}

		\Stripe\Stripe::setApiKey( $key );

		//let's check if we have plan data in cache
		$plans = get_option( 'asp_sub_plans_cache' );

		if ( ! $plans || ! isset( $plans[ $plan_id ] ) ) {

			//let's try to fetch plan by id
			try {
				$plan = \Stripe\Plan::retrieve( $plan_id );
			} catch ( EXCEPTION $e ) {
				$this->log( 'Error occurred during plan retrieve: ' . $e->getMessage(), false );
				return false;
			}
			//store plan object in cache
			if ( ! $plans ) {
				$plans = array();
			}
			$plans[ $plan_id ] = $plan;
			update_option( 'asp_sub_plans_cache', $plans );
		}
		return $plans[ $plan_id ];
	}

	public function process_charge( $data ) {
		require_once $this->PLUGIN_DIR . 'inc/asp-sub-stripe-subs-class.php';

		$this->log( 'Starting subscription process.' );

		if ( ! isset( $this->plan ) ) {

			if ( empty( $data['product_id'] ) ) {
				$this->log( 'No product ID specified.', false );
				return $data;
			}
			$id = $data['product_id'];

			$plan_id = get_post_meta( $id, 'asp_sub_plan_id', true );

			if ( ! $plan_id ) {
				$this->log( 'No plan ID specified.', false );
				return $data;
			}

			$plan = $this->get_plan_data( $plan_id );

			if ( ! $plan ) {
				$this->log( 'Can\'t get plan data.', false );
				return $data;
			}
		} else {
			$plan    = $this->plan;
			$id      = $data['product_id'];
			$plan_id = get_post_meta( $id, 'asp_sub_plan_id', true );
		}

		//handle variable currency
		if ( isset( $plan->is_variable ) && $plan->is_variable && ! empty( $plan->variables['currency'] ) ) {
			$plan->currency = sanitize_text_field( $_POST['stripeCurrency'] );
		}

		$currency_code = strtoupper( $plan->currency );

		//check if we need to apply coupon
		if ( ! empty( $data['coupon'] ) ) {
			$coupon = $data['coupon'];
			if ( $coupon['valid'] ) {
				if ( $coupon['discountType'] === 'perc' ) {
					$coupon_params = array(
						'name'        => $coupon['code'],
						'percent_off' => $coupon['discount'],
					);
				} else {
					$coupon_params = array(
						'name'       => $coupon['code'],
						'amount_off' => AcceptStripePayments::is_zero_cents( $currency_code ) ? $coupon['discountAmount'] : $coupon['discountAmount'] * 100,
						'currency'   => $currency_code,
					);
				}
				//check and set subscription-related coupon parameters
				$sub_applied_for        = get_post_meta( $coupon['id'], 'asp_sub_applied_for', true );
				$sub_applied_for        = empty( $sub_applied_for ) ? 'once' : $sub_applied_for;
				$sub_applied_for_months = get_post_meta( $coupon['id'], 'asp_sub_applied_for_months', true );
				$sub_applied_for_months = empty( $sub_applied_for_months ) ? 1 : $sub_applied_for_months;

				$coupon_params['duration'] = $sub_applied_for;
				if ( $sub_applied_for === 'repeating' ) {
					$coupon_params['duration_in_months'] = $sub_applied_for_months;
				}
				$this->log( 'Following coupon parameters applied: ' . json_encode( $coupon_params ) );
			} else {
				unset( $coupon );
			}
		}

		//check if this is variable plan
		if ( isset( $plan->is_variable ) && $plan->is_variable ) {
			//we need to create a plan first
			$ASPSUB_main = self::get_instance();
			include_once $ASPSUB_main->PLUGIN_DIR . 'inc/asp-sub-stripe-plans-class.php';

			$stripe_plans = new ASPSUB_stripe_plans( $plan->livemode );
			//let's create pricing plan in Stripe
			$plan_opts = array(
				'currency'          => $plan->currency,
				'interval'          => $plan->interval,
				'interval_count'    => $plan->interval_count,
				'amount'            => $plan->amount,
				'nickname'          => $plan->nickname,
				'trial_period_days' => 0,
			);

			$plan = $stripe_plans->create( $plan_opts, $plan->post_id );
		}

		\Stripe\Stripe::setApiKey( $plan->livemode ? $this->ASPMain->APISecKeyLive : $this->ASPMain->APISecKeyTest );

		$items = array( 'plan' => $plan->id );

		$item_quantity = false;

		if ( isset( $data['item_quantity'] ) && ! empty( $data['item_quantity'] ) ) {
			$item_quantity = intval( $data['item_quantity'] );
		}

		if ( $item_quantity ) {
			$items['quantity'] = $item_quantity;
		}

		//preload subscriptions post-related class
		$sub = new ASPSUB_stripe_subs( $plan->livemode );

		try {
			if ( ! empty( $coupon_params ) ) {
				$coupon_res = \Stripe\Coupon::create( $coupon_params );
			}

			$customer_opts = array(
				'email'  => $_POST['stripeEmail'],
				'source' => $_POST['stripeToken'],
			);

			//let's try to get customer name if available
			$name = filter_input( INPUT_POST, 'stripeBillingName', FILTER_SANITIZE_STRING );

			if ( ! empty( $name ) ) {
				$customer_opts['name'] = $name;
			}

			//let's try to get customer billing address if available
			$addr_line1 = filter_input( INPUT_POST, 'stripeBillingAddressLine1', FILTER_SANITIZE_STRING );

			if ( ! empty( $addr_line1 ) ) {
				//we have address
				$addr = array( 'line1' => $addr_line1 );

				$addr_vars = array(
					'stripeBillingAddressCity'    => 'city',
					'stripeBillingAddressZip'     => 'postal_code',
					'stripeBillingAddressState'   => 'state',
					'stripeBillingAddressCountry' => 'country',
					'stripeBillingAddressApt'     => 'line2',
				);

				foreach ( $addr_vars as $a_in => $a_out ) {
					$var = filter_input( INPUT_POST, $a_in, FILTER_SANITIZE_STRING );
					if ( ! empty( $var ) ) {
						$addr[ $a_out ] = $var;
					}
				}

				$customer_opts['address'] = $addr;
			}

			$customer = \Stripe\Customer::create( $customer_opts );

			$sub_opts = array(
				'customer'        => $customer->id,
				'items'           => array( $items ),
				'trial_from_plan' => true,
			);

			$tax = get_post_meta( $id, 'asp_product_tax', true );

			if ( ! empty( $tax ) ) {
				$sub_opts['tax_percent'] = floatval( $tax );
			}

			if ( ! empty( $coupon_res ) ) {
				$sub_opts['coupon'] = $coupon_res->id;
			}

			$subscription = \Stripe\Subscription::create( $sub_opts );
		} catch ( Exception $e ) {
			$this->log( 'Stripe API error occurred: ' . $e->getMessage(), false );
			return $data;
		}

		$sub_id = $sub->insert( $subscription, $customer, $plan_id, $id, array() );

		if ( ! empty( $coupon_res ) ) {
			$coupon_res->delete();
		}

		$this->log( sprintf( 'Customer has been successfully subscribed to plan "%s" (%s)', $plan_id, $plan->nickname ) );

		$additional_data = array();

		//check if there are cookies set by WP Affiliate Platform. If there are, we need to store them in subscription data
		if ( ! empty( $_COOKIE['ap_id'] ) ) {
			$additional_data['ap_id'] = $_COOKIE['ap_id'];
		}
		if ( ! empty( $_COOKIE['wpam_id'] ) ) {
			$additional_data['wpam_id'] = $_COOKIE['wpam_id'];
		}

		//let's check if Membership Level is set for this product
		$level_id = get_post_meta( $data['product_id'], 'asp_product_emember_level', true );
		if ( ! empty( $level_id ) ) {
			//check if we have eMember member id available
			if ( class_exists( 'Emember_Auth' ) ) {
				//Check if the user is logged in as a member.
				$emember_auth = Emember_Auth::getInstance();
				$emember_id   = $emember_auth->getUserInfo( 'member_id' );

				if ( ! empty( $emember_id ) ) {
					$additional_data['emember_id'] = $emember_id;
				}
			}
		}

		if ( ! empty( $additional_data ) ) {
			update_post_meta( $sub_id, 'sub_additional_data', $additional_data );
		}

		$data['charge'] = $subscription;

		$amount = $plan->amount;

		if ( ! empty( $plan->trial_period_days ) ) {
			$amount           = 0;
			$data['is_trial'] = true;
		}

		$item_price = $amount;

		if ( $amount > 0 ) {

			$discount_amount = ! empty( $coupon['discountAmount'] ) ? AcceptStripePayments::is_zero_cents( $currency_code ) ? $coupon['discountAmount'] : $coupon['discountAmount'] * 100 : 0;

			if ( ! empty( $discount_amount ) ) {
				$amount = $amount - $discount_amount;
			}
		}

		if ( ! empty( $tax ) ) {
			$amount = round( AcceptStripePayments::apply_tax( $amount, $tax ), 0 );
		}

		$amount = ! AcceptStripePayments::is_zero_cents( $currency_code ) ? $amount / 100 : $amount;

		if ( $item_quantity ) {
			$amount = $amount * $item_quantity;
		}

		$data['currency_code'] = strtoupper( $plan->currency );
		$data['item_price']    = ! AcceptStripePayments::is_zero_cents( $currency_code ) ? $item_price / 100 : $item_price;
		$data['paid_amount']   = $amount;

		if ( isset( $data['addonName'] ) ) {
			$data['addonName'] .= '[SUB]';
		} else {
			$data['addonName'] = '[SUB]';
		}

		$data['is_live'] = $plan->livemode ? true : false;

		return $data;
	}

	public function data_ready( $data, $atts ) {
		if ( ! isset( $data['product_id'] ) ) {
			return $data;
		}

		$id = $data['product_id'];

		$plan_id = get_post_meta( $id, 'asp_sub_plan_id', true );

		if ( ! $plan_id ) {
			return $data;
		}

		$plan = $this->get_plan_data( $plan_id );

		if ( ! ( $plan ) ) {
			return $data;
		}

		$amount = $plan->amount;

		if ( ! empty( $plan->trial_period_days ) ) {
			$data['is_trial'] = true;
		}

		$quantity = 1;

		if ( isset( $data['quantity'] ) ) {
			$quantity = intval( $data['quantity'] );
		}

		if ( $quantity ) {
			$amount = $amount * $quantity;
		}

		if ( ! empty( $data['tax'] ) ) {
			$amount = round( AcceptStripePayments::apply_tax( $amount, $data['tax'] ) );
		}

		//check if plan has variable currency
		if ( isset( $plan->is_variable ) && $plan->is_variable && ! empty( $plan->variables['currency'] ) ) {
			//it is
			$data['currency_variable'] = true;
		}

		$data['variable']        = false;
		$data['amount_variable'] = false;

		//check if plan has variable amount
		if ( isset( $plan->is_variable ) && $plan->is_variable && ! empty( $plan->variables['price'] ) ) {
			//it is
			$data['variable']        = true;
			$data['amount_variable'] = true;
		}

		if ( empty( $data['description'] ) ) {
			$data['description'] = $plan->statement_descriptor;
		}
		$data['amount']       = $amount;
		$data['item_price']   = $plan->amount;
		$data['price']        = $plan->amount;
		$data['currency']     = $plan->currency;
		$data['is_live']      = $plan->livemode ? true : false;
		$data['create_token'] = true;
		// shipping not supported
		$data['shipping'] = 0;

		return $data;
	}

	public function order_before_insert( $post, $order_details, $charge_details ) {
		if ( isset( $order_details['addonName'] ) && strpos( $order_details['addonName'], 'SUB' ) !== false ) {
			$post['post_title'] = '[SUB]' . $post['post_title'];
		}
		return $post;
	}

	public function show_user_transactions_handler( $val, $order_data, $atts ) {
		if ( $order_data['charge']->object !== 'subscription' ) {
			//not a subscription
			return $val;
		}
		include_once $this->PLUGIN_DIR . 'inc/asp-subscription-class.php';

		$sub = new ASPSUB_subscription( $order_data['charge']->id );

		if ( ! $sub ) {
			return $val;
		}

		if ( $sub->status === 'active' || $sub->status === 'trialing' ) {
			if ( ! empty( $atts['show_subscription_cancel'] ) ) {
				$cancel_url = $sub->get_cancel_link();
				$val[]      = array(
					'Subscription status:' => sprintf( '%s. <a href="%s" target="_blank">%s</a>', ucfirst( $sub->status ), $cancel_url ),
					__( 'Cancel subscription', 'asp-sub' ),
				);
			} else {
				$val[] = array( 'Subscription status:' => ucfirst( $sub->status ) );
			}
		} else {
			$val[] = array( 'Subscription status:' => ucfirst( $sub->status ) );
		}

		return $val;
	}

	public function cancel_url_handler() {
		require_once $this->PLUGIN_DIR . 'view/sub-cancel-tpl.php';
		$tpl                = new ASP_Sub_Cancel_Tpl();
		$vals['plugin_url'] = plugin_dir_url( __FILE__ ) . 'view/';
		$token              = filter_input( INPUT_GET, 'sub_token', FILTER_SANITIZE_STRING );
		if ( empty( $token ) ) {
			//no token provided
			$vals['content'] = __( 'No token provided.', 'asp-sub' );
			$tpl->set_vals( $vals );
			$tpl->display_tpl( true );
			exit;
		}
		include_once $this->PLUGIN_DIR . 'inc/asp-sub-stripe-subs-class.php';
		$subs_class = ASPSUB_stripe_subs::get_instance();
		$sub        = $subs_class->find_sub_by_token( $token );
		if ( ! $sub ) {
			//no subscription found
			$vals['content'] = __( 'No subscription found.', 'asp-sub' );
			$tpl->set_vals( $vals );
			$tpl->display_tpl( true );
			exit;
		}
		$vals['sub_token'] = $token;
		$status            = get_post_meta( $sub->ID, 'sub_status', true );
		if ( $status !== 'active' && $status !== 'trialing' && 'past_due' !== $status  && 'incomplete_expired' !== $status ) {
			//sub not active
			$vals['content'] = __( 'Subscription is not active.', 'asp-sub' );
			$tpl->set_vals( $vals );
			$tpl->display_tpl( true );
			exit;
		}
		//display subs info
		$plan_title = '';
		$plan_id    = get_post_meta( $sub->ID, 'plan_post_id', true );
		$plan       = get_post( $plan_id );
		if ( $plan ) {
			$plan_title = $plan->post_title;
		}
		$vals['content'] = sprintf( __( "Are you sure want to cancel your subscrirption to <b>%s</b>?<p>Note this can't be undone.</p>", 'asp-sub' ), $plan_title );
		$tpl->set_vals( $vals );
		$tpl->display_tpl();
		exit;
	}

	public function update_cc_url_handler() {
		require_once $this->PLUGIN_DIR . 'view/sub-update-cc-tpl.php';
		$tpl                = new ASP_Sub_Update_CC_Tpl();
		$vals['plugin_url'] = plugin_dir_url( __FILE__ ) . 'view/';
		$vals['addon_ver']  = self::ADDON_VER;
		$token              = filter_input( INPUT_GET, 'sub_token', FILTER_SANITIZE_STRING );
		if ( empty( $token ) ) {
			//no token provided
			$vals['content'] = __( 'No token provided.', 'stripe-paymets' );
			$tpl->set_vals( $vals );
			$tpl->display_tpl( true );
			exit;
		}
		include_once $this->PLUGIN_DIR . 'inc/asp-sub-stripe-subs-class.php';

		$subs_class = ASPSUB_stripe_subs::get_instance();
		$sub        = $subs_class->find_sub_by_token( $token );
		if ( ! $sub ) {
			//no subscription found
			$vals['content'] = __( 'No subscription found.', 'asp-sub' );
			$tpl->set_vals( $vals );
			$tpl->display_tpl( true );
			exit;
		}
		$status = get_post_meta( $sub->ID, 'sub_status', true );
		if ( $status !== 'active' && $status !== 'trialing' && 'past_due' !== $status  && 'incomplete_expired' !== $status ) {
			//sub not active
			$vals['content'] = __( 'Subscription is not active.', 'asp-sub' );
			$tpl->set_vals( $vals );
			$tpl->display_tpl( true );
			exit;
		}

		$use_old_api = $this->ASPMain->get_setting( 'use_old_checkout_api1' );

		$is_live = get_post_meta( $sub->ID, 'is_live', true );

		$subs_class = new ASPSUB_stripe_subs( $is_live );

		$customer_id = get_post_meta( $sub->ID, 'cust_id', true );

		if ( empty( $customer_id ) ) {
			//no customer id found
			$vals['content'] = __( 'No customer ID found for this subscription.', 'asp-sub' );
			$tpl->set_vals( $vals );
			$tpl->display_tpl( true );
			exit;
		}

		$key = $is_live ? $this->ASPMain->APIPubKey : $this->ASPMain->APIPubKeyTest;

		$cu = \Stripe\Customer::Retrieve(
			array(
				'id'     => $customer_id,
				'expand' => array( 'default_source', 'invoice_settings' ),
			)
		);

		if ( $use_old_api ) {

			$stripeToken = filter_input( INPUT_POST, 'stripeToken', FILTER_SANITIZE_STRING );

			if ( ! empty( $stripeToken ) ) {
				try {
					$cu = \Stripe\Customer::update(
						$customer_id,
						array( 'source' => $stripeToken )
					);
				} catch ( Exception $e ) {

					// Use the variable $error to save any errors
					// To be displayed to the customer later in the page
					$vals['content'] = __( 'Error occurred:', 'asp-sub' ) . ' ' . $e->getMessage();
					$tpl->set_vals( $vals );
					$tpl->display_tpl( true );
					exit;
				}
				//update was successful. Let's remove customer_cc_expiring flag
				update_post_meta( $sub->ID, 'customer_cc_expiring', false );
				$vals['content'] = __( 'Your card details have been updated!', 'asp-sub' );
				$tpl->set_vals( $vals );
				$tpl->display_tpl();
				exit;
			}
		} else {
			$pm_id = filter_input( INPUT_POST, 'pm_id', FILTER_SANITIZE_STRING );
			if ( ! empty( $pm_id ) ) {
				try {
					$payment_method = \Stripe\PaymentMethod::retrieve( $pm_id );
					$payment_method->attach( array( 'customer' => $customer_id ) );
					$cu = \Stripe\Customer::update(
						$customer_id,
						array(
							'invoice_settings' => array(
								'default_payment_method' => $pm_id,
							),
						)
					);
				} catch ( Exception $e ) {

					// Use the variable $error to save any errors
					// To be displayed to the customer later in the page
					$vals['content'] = __( 'Error occurred:', 'asp-sub' ) . ' ' . $e->getMessage();
					$tpl->set_vals( $vals );
					$tpl->display_tpl( true );
					exit;
				}
				//update was successful. Let's remove customer_cc_expiring flag
				update_post_meta( $sub->ID, 'customer_cc_expiring', false );
				$vals['content'] = __( 'Your card details have been updated!', 'asp-sub' );
				$tpl->set_vals( $vals );
				$tpl->display_tpl();
				exit;
			}
		}

		if ( $use_old_api ) {
			$def_brand = $cu->default_source->brand;
			$def_last4 = $cu->default_source->last4;
		} else {
			$pm_id     = $cu->invoice_settings->default_payment_method;
			$pm        = \Stripe\PaymentMethod::retrieve( $pm_id );
			$def_brand = $pm->card->brand;
			$def_last4 = $pm->card->last4;
		}

		$content = '<p>' . sprintf( __( 'Current card on file: %1$s ending in %2$d', 'asp-sub' ), $def_brand, $def_last4 ) . '</p>';

		$email = $cu->email;
		ob_start();

		if ( $use_old_api ) {
			?>
		<form action="" method="POST">
			<script src="https://checkout.stripe.com/checkout.js" class="stripe-button" data-key="<?php echo $key; ?>" data-name="<?php _e( 'Update Card Details', 'asp-sub' ); ?>" data-panel-label="<?php _e( 'Update Card Details', 'asp-sub' ); ?>" data-label="<?php _e( 'Update Card Details', 'asp-sub' ); ?>" data-allow-remember-me="false" data-locale="auto" data-email="<?php echo $email; ?>">
			</script>
		</form>
			<?php
		} else {
			$vars = array(
				'ajaxUrl' => admin_url( 'admin-ajax.php' ),
				'custId'  => $customer_id,
				'key'     => $key,
				'isLive'  => $is_live,
			);
			?>
			<form id="update-form" class="pure-form pure-form-stacked" action="" method="POST">
			<div id="error-cont"></div>
			<fieldset>
				<input type="hidden" name="new_api" value="1">
				<input type="hidden" id="pm_id" name="pm_id" value="">
				<label for="card-element"><?php esc_html_e( 'Credit or debit card', 'stripe-payments' ); ?></label>
				<div id="card-element"></div>
				<div id="card-errors" class="form-err" role="alert"></div>
			</fieldset>
				<button type="submit" id="submitBtn" class="pure-button pure-button-primary" disabled><?php esc_attr_e( 'Update Card Details', 'asp-sub' ); ?></button>
			</form>
			<script>var vars=<?php echo wp_json_encode( $vars ); ?>;</script>
			<script src="https://js.stripe.com/v3/"></script>
			<script src="<?php echo esc_url( plugin_dir_url( __FILE__ ) ); ?>view/js/asp-sub-utils.js?ver=<?php echo esc_js( self::ADDON_VER ); ?>"></script>
			<script src="<?php echo esc_url( plugin_dir_url( __FILE__ ) ); ?>view/js/asp-sub-update-cc-handler.js?ver=<?php echo esc_js( self::ADDON_VER ); ?>"></script>
			<?php
		}
		$content        .= ob_get_clean();
		$vals['content'] = $content;
		$tpl->set_vals( $vals );
		$tpl->display_tpl();
		exit;
	}

	public function process_subs_cancel() {
		$out['success'] = false;
		$token          = filter_input( INPUT_POST, 'subId', FILTER_SANITIZE_STRING );
		include_once $this->PLUGIN_DIR . 'inc/asp-sub-stripe-subs-class.php';
		$subs_class = ASPSUB_stripe_subs::get_instance();
		$sub        = $subs_class->find_sub_by_token( $token );
		if ( ! $sub ) {
			//no subscription found
			$out['msg'] = __( 'No subscription found.', 'asp-sub' );
			wp_send_json( $out );
			exit;
		}
		$status = get_post_meta( $sub->ID, 'sub_status', true );
		if ( $status !== 'active' && $status !== 'trialing' && 'past_due' !== $status  && 'incomplete_expired' !== $status) {
			//sub not active
			$out['msg'] = __( 'Subscription is not active.', 'asp-sub' );
			wp_send_json( $out );
			exit;
		}
		$is_live    = get_post_meta( $sub->ID, 'is_live', true );
		$subs_class = new ASPSUB_stripe_subs( $is_live );
		$res        = $subs_class->cancel( $sub->ID );
		if ( ! $res ) {
			//error occurred during cancel
			$out['msg'] = $subs_class->get_last_error();
			wp_send_json( $out );
			exit;
		}
		$out['success'] = true;
		$out['msg']     = __( 'Subscription has been cancelled.', 'asp-sub' );
		wp_send_json( $out );
		exit;
	}

	public function email_body_tags_vals_before_replace( $tags_vals, $post ) {
		if ( ! isset( $post['charge']->object ) || $post['charge']->object != 'subscription' ) {
			//not a subscription
			return $tags_vals;
		}
		$sub_id = $post['charge']->id;
		include_once $this->PLUGIN_DIR . 'inc/asp-sub-stripe-subs-class.php';
		$sub_class     = ASPSUB_stripe_subs::get_instance();
		$cancel_url    = $sub_class->get_cancel_link( $sub_id );
		$update_cc_url = $sub_class->get_update_cc_link( $sub_id );
		if ( $cancel_url ) {
			//let's add tags and corresponding vals
			$tags_vals['tags'][] = '{sub_cancel_url}';
			$tags_vals['vals'][] = $cancel_url;
			$tags_vals['tags'][] = '{sub_update_cc_url}';
			$tags_vals['vals'][] = $update_cc_url;
		}
		return $tags_vals;
	}

	public function email_body_after_replace( $body ) {
		//let's remove potential tags leftovers
		$body = preg_replace( array( '/\{sub_cancel_url\}/' ), array( '' ), $body );
		$body = preg_replace( array( '/\{sub_update_cc_url\}/' ), array( '' ), $body );
		return $body;
	}


	public function log( $msg, $success = true ) {
		if ( method_exists( 'ASP_Debug_Logger', 'log' ) ) {
			ASP_Debug_Logger::log( $msg, $success, $this->ADDON_SHORT_NAME );
		}
	}

	public static function get_plugin_dir() {
		return plugin_dir_path( __FILE__ );
	}

	public function create_si() {
		$out        = array();
		$out['err'] = '';
		$cust_id    = filter_input( INPUT_POST, 'cust_id', FILTER_SANITIZE_STRING );
		$token      = filter_input( INPUT_POST, 'token_id', FILTER_SANITIZE_STRING );
		$is_live    = filter_input( INPUT_POST, 'is_live', FILTER_SANITIZE_STRING );
		if ( empty( $cust_id ) ) {
			$out['err'] = 'Invalid customer ID provided';
			wp_send_json( $out );
			exit;
		}
		if ( empty( $token ) ) {
			$out['err'] = 'Invalid card token provided';
			wp_send_json( $out );
			exit;
		}

		ASPMain::load_stripe_lib();

		\Stripe\Stripe::setApiKey( $is_live ? $this->ASPMain->APISecKeyLive : $this->ASPMain->APISecKeyTest );

		try {
			$si = \Stripe\SetupIntent::create(
				array(
					'customer'            => $cust_id,
					'payment_method_data' => array(
						'type' => 'card',
						'card' => array( 'token' => $token ),
					),
				)
			);
		} catch ( Exception $e ) {
			$out['err'] = $e->getMessage();
			wp_send_json( $out );
		}

		$out['si_id'] = $si->id;
		$out['si_cs'] = $si->client_secret;
		//      $out['si'] = wp_json_encode($si);

		wp_send_json( $out );
	}
}

$ASPSUB_main = new ASPSUB_main();
