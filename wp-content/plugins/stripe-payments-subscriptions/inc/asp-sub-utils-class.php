<?php

class ASPSUB_Utils {

    protected static $instance	 = null;
    private $ASPMain;
    private $lastSignSec		 = false;
    private $lastStatus;
    private $lastMsg;
    private $lastWebhook;
    private $lastHideBtn;
    private $showLiveHookNotice	 = false;
    private $showTestHookNotice	 = false;
    public $oldCorePluginVersion	 = false;

    public static function get_instance() {

	if ( null == self::$instance ) {
	    self::$instance = new self;
	}

	return self::$instance;
    }

    function __construct() {
	$this->ASPMain = AcceptStripePayments::get_instance();
	ASPMain::load_stripe_lib();
	if ( wp_doing_ajax() && is_admin() ) {
	    $this->add_ajax_hooks();
	}

	$this->oldCorePluginVersion = version_compare( WP_ASP_PLUGIN_VERSION, '1.9.15t1' ) < 0 ? true : false;

	if ( ! $this->oldCorePluginVersion ) {
	    //schedule event to check if webhooks are configured properly
	    add_action( 'asp_sub_check_webhooks_status', array( $this, 'check_webhooks_status' ) );
	    if ( ! wp_next_scheduled( 'asp_sub_check_webhooks_status' ) ) {
		wp_schedule_event( time(), 'twicedaily', 'asp_sub_check_webhooks_status' );
	    }
	}
	if ( is_admin() ) {
	    $this->showLiveHookNotice	 = get_option( 'asp_sub_show_live_webhook_notice' );
	    $this->showTestHookNotice	 = get_option( 'asp_sub_show_test_webhook_notice' );
	    if ( $this->showLiveHookNotice || $this->showTestHookNotice ) {
		add_action( 'admin_notices', array( $this, 'show_webhooks_admin_notice' ) );
	    }
	}
    }

    public function process_deactivate_hook() {
	$timestamp = wp_next_scheduled( 'asp_sub_check_webhooks_status' );
	wp_unschedule_event( $timestamp, 'asp_sub_check_webhooks_status' );
    }

    public function show_webhooks_admin_notice() {
	$class		 = 'notice notice-error';
	$settings_url	 = add_query_arg( array(
	    'post_type'	 => 'asp-products',
	    'page'		 => 'stripe-payments-settings#sub',
	), get_admin_url() . 'edit.php' );
	$message	 = sprintf( __( '<b>Stripe Payments Subscriptions Addon:</b> webhooks seem to be not configured properly. Please go to <a href="%s">settings page</a> to configure them.', 'asp-sub' ), $settings_url );
	printf( '<div class="%1$s"><p>%2$s</p></div>', esc_attr( $class ), $message );
    }

    public function check_webhooks_status() {
	if ( $this->oldCorePluginVersion ) {
	    return false;
	}
	$live = $this->check_webhook( 'live' );
	if ( ! $live ) {
	    // no live webhook created
	    update_option( 'asp_sub_show_live_webhook_notice', true );
	} else {
	    update_option( 'asp_sub_show_live_webhook_notice', false );
	}
	$test = $this->check_webhook( 'test' );
	if ( ! $test ) {
	    // no test webhook created
	    update_option( 'asp_sub_show_test_webhook_notice', true );
	} else {
	    update_option( 'asp_sub_show_test_webhook_notice', false );
	}
    }

    static function get_webhook_url( $mode ) {
	$webhook_url = add_query_arg( array(
	    'asp_hook' => $mode,
	), get_home_url( null, '/', 'https' ) );
	return $webhook_url;
    }

    private function add_ajax_hooks() {
	if ( current_user_can( 'manage_options' ) ) {
	    add_action( 'wp_ajax_asp_sub_clear_cache', array( $this, 'ajax_clear_plans_cache' ) );
	    add_action( 'wp_ajax_asp_sub_check_webhooks', array( $this, 'ajax_check_webhooks' ) );
	    add_action( 'wp_ajax_asp_sub_create_webhook', array( $this, 'ajax_create_webhook' ) );
	    add_action( 'wp_ajax_asp_sub_delete_webhooks', array( $this, 'ajax_delete_webhooks' ) );
	}
    }

    public function ajax_clear_plans_cache() {
	update_option( 'asp_sub_plans_cache', '' );
	$ret[ 'msg' ] = __( 'Cache has been cleared.', 'asp-sub' );
	wp_send_json( $ret );
    }

    private function find_webhook( $webhooks, $url ) {
	$url = strtolower( $url );
	foreach ( $webhooks as $webhook ) {
	    if ( strtolower( $webhook[ 'url' ] ) === $url ) {
		return $webhook;
	    }
	}
	return false;
    }

    private function check_webhook( $mode ) {
	$this->lastHideBtn	 = false;
	$this->lastWebhook	 = false;
	$key			 = $mode === 'live' ? $this->ASPMain->APISecKeyLive : $this->ASPMain->APISecKeyTest;
	try {
	    \Stripe\Stripe::setApiKey( $key );
	    $webhooks = \Stripe\WebhookEndpoint::all( [ "limit" => 100 ] );
	} catch ( \Stripe\Error\Authentication $e ) {
	    $this->lastStatus	 = 'no';
	    $this->lastMsg		 = sprintf( __( 'Invalid API Key. Please check core plugins settings and enter valid key for %s mode.', 'asp-sub' ), $mode );
	    $this->lastHideBtn	 = true;
	    return false;
	} catch ( Exception $e ) {
	    $this->lastStatus	 = 'no';
	    $this->lastMsg		 = $e->getMessage();
	    $this->lastHideBtn	 = true;
	    return false;
	}
	if ( ! empty( $webhooks->data[ 0 ] ) ) {
	    //we have some webhooks set. Let's find if ours is there
	    $webhook = $this->find_webhook( array( $webhooks->data[ 0 ] ), self::get_webhook_url( $mode ) );
	    if ( $webhook !== false ) {
		//webhook already exists
		$this->lastStatus	 = 'yes';
		$this->lastMsg		 = __( 'Webhook exists. If you still have issues with webhooks, try to delete them and create again.', 'asp-sub' );
		$this->lastHideBtn	 = true;
		$this->lastWebhook	 = $webhook;
		update_option( 'asp_sub_show_' . $mode . '_webhook_notice', false );
		return true;
	    }
	}
	//webhook wasn't found
	$this->lastStatus	 = 'no';
	$this->lastMsg		 = __( 'No webhook found. Use the button below to automatically create it.', 'asp-sub' );
	$this->lastSignSec	 = '';

	return false;
    }

    private function create_webhook( $mode ) {
	try {
	    $webhook				 = \Stripe\WebhookEndpoint::create( array(
		"url"		 => self::get_webhook_url( $mode ),
		"enabled_events" => array( "*" )
	    ) );
	    //let's also update webhook signing secret
	    unregister_setting( 'AcceptStripePayments-settings-group', 'AcceptStripePayments-settings' );
	    $opts					 = get_option( 'AcceptStripePayments-settings' );
	    $opts[ $mode . '_webhook_secret' ]	 = $webhook->secret;
	    update_option( 'AcceptStripePayments-settings', $opts );
	    $this->lastSignSec			 = $webhook->secret;
	    return true;
	} catch ( Exception $e ) {
	    $this->lastMsg = __( 'Error occurred during webhook creation:', 'asp-sub' ) . ' ' . $e->getMessage();
	    return false;
	}
    }

    public function ajax_check_webhooks() {
	$ret = array();
	//check core plugin version first
	if ( $this->oldCorePluginVersion ) {
	    //core plugin version is too old for this functionality
	    $msg				 = __( 'Stripe Payments core plugin version 1.9.15+ required for this functionality', 'asp-sub' );
	    $ret[ 'live' ][ 'status' ]	 = 'warning';
	    $ret[ 'live' ][ 'msg' ]		 = $msg;
	    $ret[ 'test' ][ 'status' ]	 = 'warning';
	    $ret[ 'test' ][ 'msg' ]		 = $msg;
	    $ret[ 'test' ][ 'hidebtn' ]	 = true;
	    $ret[ 'live' ][ 'hidebtn' ]	 = true;
	    wp_send_json( $ret );
	}
	//check and create webhooks
	$this->check_webhook( 'test' );
	$ret[ 'test' ][ 'status' ]	 = $this->lastStatus;
	$ret[ 'test' ][ 'msg' ]		 = $this->lastMsg;
	$ret[ 'test' ][ 'hidebtn' ]	 = $this->lastHideBtn;
	$ret[ 'test' ][ 'signsec' ]	 = $this->lastSignSec;
	$this->check_webhook( 'live' );
	$ret[ 'live' ][ 'status' ]	 = $this->lastStatus;
	$ret[ 'live' ][ 'msg' ]		 = $this->lastMsg;
	$ret[ 'live' ][ 'hidebtn' ]	 = $this->lastHideBtn;
	$ret[ 'live' ][ 'signsec' ]	 = $this->lastSignSec;
	wp_send_json( $ret );
    }

    public function ajax_create_webhook() {
	$mode = filter_input( INPUT_GET, 'mode', FILTER_SANITIZE_STRING );
	if ( ! in_array( $mode, array( 'test', 'live' ) ) ) {
	    //invalid mode specified
	    $ret[ 'status' ] = 'no';
	    $ret[ 'msg' ]	 = __( "Invalid mode specified", 'asp-sub' );
	    wp_send_json( $ret );
	}
	$webhook = $this->check_webhook( $mode );
	if ( $webhook ) {
	    $ret[ 'status' ]	 = 'yes';
	    $ret[ 'msg' ]		 = __( "Webhook exists", 'asp-sub' );
	    $ret[ 'hidebtn' ]	 = true;
	    wp_send_json( $ret );
	}
	if ( $this->create_webhook( $mode ) ) {
	    //webhook created
	    $ret[ 'status' ]	 = 'yes';
	    $ret[ 'signsec' ]	 = $this->lastSignSec;
	    $ret[ 'hidebtn' ]	 = true;
	    update_option( 'asp_sub_show_' . $mode . '_webhook_notice', false );
	    $ret[ 'msg' ]		 = __( "Webhook has been created", 'asp-sub' );
	} else {
	    //error occurred during webhook creation
	    $ret[ 'status' ]	 = 'no';
	    $ret[ 'signsec' ]	 = $this->lastMsg;
	}
	wp_send_json( $ret );
    }

    public function ajax_delete_webhooks() {
	$this->check_webhook( 'test' );
	if ( $this->lastWebhook != false ) {
	    $this->lastWebhook->delete();
	}
	$this->check_webhook( 'live' );
	if ( $this->lastWebhook != false ) {
	    $this->lastWebhook->delete();
	}
	$ret[ 'success' ]	 = true;
	$ret[ 'msg' ]		 = "Webhooks have been deleted.";
	wp_send_json( $ret );
    }

    static function format_date( $date ) {
	$format		 = get_option( 'date_format' ) . ', ' . get_option( 'time_format' );
	$tmp_date	 = date( 'Y-m-d H:i:s', $date );
	$ret		 = get_date_from_gmt( $tmp_date, $format );
	return $ret;
    }

    static function get_interval_str( $unit, $count = 0 ) {
	$period_units_str = array(
	    'days'	 => array( __( 'day', 'asp-sub' ), __( 'days', 'asp-sub' ) ),
	    'weeks'	 => array( __( 'week', 'asp-sub' ), __( 'weeks', 'asp-sub' ) ),
	    'months' => array( __( 'month', 'asp-sub' ), __( 'months', 'asp-sub' ) )
	);
	if ( ! isset( $period_units_str[ $unit ] ) ) {
	    return $unit;
	}
	$ret = $unit;
	if ( $count == 1 ) {
	    $ret = $period_units_str[ $unit ][ 0 ];
	} else {
	    $ret = $period_units_str[ $unit ][ 1 ];
	}
	return $ret;
    }

}

ASPSUB_Utils::get_instance();
