<?php

class ASP_Sub_Product_Item extends ASP_Product_Item {

	protected $plan_id = false;

	public function __construct( $post_id = false ) {
		$this->asp_main = AcceptStripePayments::get_instance();
		if ( false !== $post_id ) {
			//let's try to load item from product
			$this->post_id = $post_id;
			$this->load_from_product();
			if ( ! $this->last_error ) {
				//check if this is subs product
				$plan_id = get_post_meta( $post_id, 'asp_sub_plan_id', true );
				if ( ! $plan_id ) {
					$this->last_error = 'Not subscription product.';
				} else {
					$this->plan_id = $plan_id;
				}
			}
		}
	}

	public function get_plan_id() {
		return $this->plan_id;
	}

	public function get_shipping( $in_cents = false ) {
		//shipping not supported for subscriptions yet
		return 0;
	}

}
